/*
 * Dany Turcios
 * This is a project hangman game that i added a few edits to
 */
import java.util.Scanner;
public class HangmanMain {

	public static void main(String[] args) {
		Scanner keyboard= new Scanner(System.in);
			String[]listWord = {"luffy","nami","haki","zoro","usopp","dragon","pirate","whitebeard","blackbeard","sanji","devilfruit","worstgeneration","franky","chopper","robin","brooke","garp","bellamy","dofalmingo","youkai"};//Array of possible strings to choose from
			String response = "yes";
			String answer = " ";
			int compWins =0, userWins=0, guessesLeft;
			String userWordGuess=" ";
			boolean wordCheck,completeWord;
		do{
			answer=listWord[compWord(listWord)];//Sets random compWord equal to a string variable at beginning of each game
			char[]userGuess = new char [answer.length()];
			if (response.equalsIgnoreCase("yes")){//Begin game
				System.out.println("Welcome to the One Piece Hangman Word game!");
				userWordDisplay(answer,userGuess);//Displays user's version of word
				completeWord=false;//makes sure that when loop returns, it is returned to false
				guessesLeft=6;//returns your guesses
				do{
					System.out.println("Choose a letter or enter zero to guess the word:");
					char userInput=keyboard.next().charAt(0);//removes white space and uses first character inputed
					if(userInput=='0'){//User inputs 0 so they must guess the word
						System.out.println("Guess the word: ");
						userWordGuess = keyboard.next();
						wordCheck=userWordCheck(userWordGuess, answer);//Checks user word for correctness
							if (wordCheck== true){
							System.out.println("You guessed the word. That is correct.");
							++userWins;//Increase user win by 1
							completeWord=true;
							}
							else if(wordCheck==false){
							System.out.println("You guessed the word. That is not correct.");
							++compWins;//Increase computer win by 1
							completeWord=true;
							}
					}
					else{//else if user did not input '0'
						if(answer.indexOf(userInput)>=0) {//If the char is within the string
							userWordCharReplacement(userInput,answer,userGuess);//Call to method that inputs char into user version of Word
							boolean fullWord=compUserWord(userGuess,answer);//checks if chars have been guessed
							if (fullWord== true){
								System.out.println("You guessed the word. That is correct.");
								++userWins;
								completeWord=true;
							}
							else if(fullWord==false){//If full word has not been guessed yet then continue asking for guesses
								completeWord=false;
							}	
						}
						else{//If char is not within the string
					guessesLeft=guessesLeft-1;//incorrect guess decreases your guesses left
					System.out.println("This is not in the word. You have "+guessesLeft+" guesses remaining.");
							if(guessesLeft==0){
								System.out.println("You must guess the word now: ");
								userWordGuess = keyboard.next();
								wordCheck=userWordCheck(userWordGuess, answer);//call to function that checks user word
									if (wordCheck== true){
										System.out.println("You guessed the word. That is correct.");
										++userWins;
										completeWord=true;
									}
									else if(wordCheck==false){
										System.out.println("You guessed the word. That is not correct.");
										++compWins;
										completeWord=true;
									}	
							}
							else{						
							}
						}
					}					
				}while(completeWord==false);//End of second do-while. Ends Game
				System.out.println("Do you want to play again?");
				response = keyboard.next();
			}		
			else if(response.equalsIgnoreCase("no")){//Exits game				
			}
			else{//Error message if user does not input yes or no
				System.out.println("I am sorry that was not a valid answer");
				System.out.println("Do you want to play again?");
				response = keyboard.next();				
				}			
		}while(response.equalsIgnoreCase("no")==false);//End of first do-while loop
			System.out.println("Computer wins: "+compWins);
			System.out.println("Player wins: "+userWins);
			System.out.println("Thanks for playing! Bye.");		
	}
	public static int compWord(String listWords []){//FUNCTION to select compWord
		int compChoice;
		compChoice =(int)(Math.random()*listWords.length);//Draws a random number from 0-19 (array of compWord index)
		return compChoice;
	}
	public static boolean userWordCheck(String wordGuess, String realWord){//FUNCTION to check if user word guess is correct
		if(realWord.equalsIgnoreCase(wordGuess)){
			return true;
		}
		else{
			return false;
		}
	}
	public static void userWordCharReplacement(char userGuess1,String compWord1,char[]userWord){//METHOD for players version of word
		char [] compWord2=compWord1.toCharArray();//Turns string into char array
		for(int i=0;i<compWord1.length();++i){
				if(compWord2[i]==userGuess1){
				userWord[i]=userGuess1;
				System.out.print(userWord[i]);
			}
				else if (userWord[i]==compWord2[i]) {//Makes sure any previous guesses remain
				userWord[i]=compWord2[i];
				System.out.print(userWord[i]);
			}				
				else if(userGuess1!=compWord2[i]&&compWord2[i]!=userWord[i]){//Anything that has not been guessed yet remains a hyphen
				userWord[i]='-';
				System.out.print(userWord[i]);
			}
		}
		System.out.println(" ");	
	}				
	public static void userWordDisplay(String answer1,char[]userGuess1){//Method to display user's word
		char[]answerArray=answer1.toCharArray();
		char[]userVersionWord=new char[answer1.length()];//Users Version of Word
		for(int i=0;i<answer1.length();i++){//Goes through each char of answer1
			for(int j=0;j<userGuess1.length;j++){//For each char of answer1, userGuess1 is checked
				if(userGuess1[j]==answerArray[i]){
					userGuess1[j]=userVersionWord[i];
				}
				else if(userGuess1[j]!=answerArray[i]&&userVersionWord[i]!=answerArray[i]){
					userVersionWord[i]='-';
				}
			}
		}
		System.out.println(userVersionWord);
	}
	public static boolean compUserWord(char[]userWord1,String compWord1){//Function to determine if the full word has been guessed
		String userWord1String = new String (userWord1);//Returns char array to String to check for completeness
		if(userWord1String.equals(compWord1)){
			return true;
		}
		else{
			return false;
	}
	}
}